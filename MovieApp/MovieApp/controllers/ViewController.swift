//
//  ViewController.swift
//  MovieApp
//
//  Created by admin on 2018-10-24.
//  Copyright © 2018 admin. All rights reserved.
//

import UIKit


extension ViewController{

    func generateData()->[Movie]? {
        let m1 = Movie(image: "img1", title: "title1", desc: "desc1")
        let m2 = Movie(image: "img2", title: "title2", desc: "desc2")
        let m3 = Movie(image: "img3", title: "title3", desc: "desc3")
        let m4 = Movie(image: "img4", title: "title4", desc: "desc4")
        let m5 = Movie(image: "img5", title: "title5", desc: "desc5")
        let m6 = Movie(image: "img6", title: "title6", desc: "desc6")
        let m7 = Movie(image: "img7", title: "title7", desc: "desc7")
        
        return [m1, m2, m3, m4, m5, m6, m7]
    }
    
}

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, MovieDetailViewControllerProtocol {

    @IBOutlet weak var movieTableView: UITableView!
    
    var movieList : [Movie]?
    var lastSelectedRow: Int = 0

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.movieTableView.delegate = self
        self.movieTableView.dataSource=self;
        
        self.movieList = self.generateData()
        self.movieTableView.reloadData()
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    

    //-- MARK delegate methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section==0
        {
            return self.movieList?.count ?? 0
        }
        else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var tableviewCell:MovieTableViewCell?

        tableviewCell = tableView.dequeueReusableCell(withIdentifier: "CustomMovieCellIdentifier") as? MovieTableViewCell
        
        if tableviewCell==nil
        {
            tableviewCell = MovieTableViewCell(style: UITableViewCell.CellStyle.subtitle, reuseIdentifier: "CustomMovieCellIdentifier")
        }
        
//        if indexPath.row%2 == 0
//        {
//            tableviewCell = tableView.dequeueReusableCell(withIdentifier: "CustomMovieCellIdentifier") as? MovieTableViewCell
//
//            if tableviewCell==nil
//            {
//                tableviewCell = UITableViewCell(style: UITableViewCell.CellStyle.subtitle, reuseIdentifier: "CustomMovieCellIdentifier") as? MovieTableViewCell
//            }
//        }
//        else
//        {
//            tableviewCell = tableView.dequeueReusableCell(withIdentifier: "movieCell") as? MovieTableViewCell
//
//            if tableviewCell==nil
//            {
//                tableviewCell = UITableViewCell(style: UITableViewCell.CellStyle.subtitle, reuseIdentifier: "movieCell")
//            }
//        }
     
        
//        tableviewCell?.detailTextLabel?.text = "Movie Detail"
//        tableviewCell?.textLabel?.text = "Text Label"
//        tableviewCell?.textLabel?.textColor = .white
//        tableviewCell?.textLabel?.backgroundColor = .yellow
//        tableviewCell?.imageView?.image = UIImage(named: "star1")

        tableviewCell?.titleLabel.text = self.movieList?[indexPath.row].title
        //tableviewCell?.movi?.text = self.movieList?[indexPath.row].title
        //tableviewCell?.textLabel?.text = self.movieList?[indexPath.row].desc
        tableviewCell?.descLabel?.text = self.movieList?[indexPath.row].desc
        tableviewCell?.titleLabel?.textColor = .white
        tableviewCell?.descLabel?.backgroundColor = .yellow
        
        if let imgurl = self.movieList?[indexPath.row].image
        {
            tableviewCell?.movieImgView?.image = UIImage(named: imgurl)
        }
        
        return tableviewCell!
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath.section==0
        {
            return 100
        }
        else
        {
            return 80
        }
//        if indexPath.row%2==0
//        {
//            return 60
//        }
//        else {
//            return 100
//        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let header: MovieSectionHeaderView =  MovieSectionHeaderView.instanceFromNib() as! MovieSectionHeaderView
       
        header.frame = CGRect(x: 0, y: 0, width: self.movieTableView.frame.size.width, height: 40)
        
        header.titleLabel.text = "Sercan"
        return header
    }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let deleteAction = UIContextualAction(style: .destructive, title: "Delete") { (action, view, handler) in
            print("Delete Action Tapped")
        }
        deleteAction.backgroundColor = .red
        let configuration = UISwipeActionsConfiguration(actions: [deleteAction])
        return configuration
    }
    func tableView(_ tableView: UITableView, leadingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        
        let deleteAction = UIContextualAction(style: .destructive, title: "Add") { (action, view, handler) in
            print("Add Action Tapped")
        }
        deleteAction.backgroundColor = .green
        let configuration = UISwipeActionsConfiguration(actions: [deleteAction])
        return configuration
        
    }

//    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
//
//        let more = UITableViewRowAction(style: .normal, title: "More") { action, index in
//            print("more button tapped")
//        }
//        more.backgroundColor = .lightGray
//
//        let favorite = UITableViewRowAction(style: .normal, title: "Favorite") { action, index in
//            print("favorite button tapped")
//        }
//        favorite.backgroundColor = .orange
//
//        let share = UITableViewRowAction(style: .normal, title: "Share") { action, index in
//            print("share button tapped")
//        }
//        share.backgroundColor = .blue
//
//        return [share, favorite, more]
//    }
//
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {

        return true
    }
    
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
        return UITableViewCell.EditingStyle.insert
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
//        let row: Int = indexPath.row
//        let section: Int = indexPath.section

//        let rs: String = String(row+1)
//        let ss: String = String(section+1)

//        let alert = UIAlertController(title: "\(rs)->\(ss)", message: "message", preferredStyle: UIAlertController.Style.alert)

//        let cancelAction = UIAlertAction(title: "Close", style: .default) { (result : UIAlertAction) -> Void in
//            alert.dismiss(animated: true, completion: nil)
//        }
//        alert.addAction(cancelAction)
//        self.present(alert, animated: true, completion: nil)
    }


     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {

//        let x = segue.destination as? UINavigationController
//
//        let y = x?.topViewController as! MovieDetailViewController

        let x = segue.destination as? MovieDetailViewController

        x?.delegate = self
        if let s = sender
        {
            let row = self.movieTableView.indexPath(for: s as! MovieTableViewCell)?.row
            self.lastSelectedRow = row ?? 0

            if let mlist = movieList, let r = row
            {
                let m = mlist[r]
                //y.movie = m
                x?.movie = m

                x?.navigationItem.title = m.title
            }
        }
     }

    func movieDidGetUpdated(movieDtailViewController: MovieDetailViewController, movie: Movie) {
        movieList?.remove(at: self.lastSelectedRow)
        movieList?.insert(movie, at: self.lastSelectedRow)

        self.movieTableView.reloadData()
    }
}

